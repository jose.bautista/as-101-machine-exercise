package com.example.newapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.fragment_step1.view.*


class step1Fragment : Fragment() {
    private lateinit var communicator: Communicator
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_step1, container, false)


        view.button1.setOnClickListener {

            val first_name = view.first_name_edit.text.toString()
            val last_name = view.last_name_edit.text.toString()
            val birthday = view.birthday_edit.text.toString()



            val action  = step1FragmentDirections.step1ToStep2(first_name,last_name,birthday)
            Navigation.findNavController(view).navigate(action)


        }
        return view
    }
}