package com.example.newapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_step2.view.*
import kotlinx.android.synthetic.main.fragment_step3.view.*

class step3Fragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_step3, container, false)

        view.button5.setOnClickListener{ Navigation.findNavController(view).navigate(R.id.step3_to_step1)}
        return view
    }
}