package com.example.newapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_step1.view.*
import kotlinx.android.synthetic.main.fragment_step2.view.*

class step2Fragment : Fragment() {

    val args: step2FragmentArgs by navArgs()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_step2, container, false)

        val firstName = args.firstName
        val lastName = args.lastName
        val birthday = args.birthday

        view.textFirstName.setText(firstName)
        view.textLastName.setText(lastName)
        view.textBirthday.setText(birthday)

        view.button3.setOnClickListener{ Navigation.findNavController(view).navigate(R.id.step2_to_step1)}
        view.button4.setOnClickListener{ Navigation.findNavController(view).navigate(R.id.step2_to_step3)}

        return view
    }

}